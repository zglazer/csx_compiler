///////////////////////////////////////////////////////////////////////////////
//
// Title:            ErrMsg.java
//
// Semester:         CS536 Spring 2014
//
// Author:           Zach Glazer
// Email:            zglazer@wisc.edu
// CS Login:         glazer
// Lecturer's Name:  Hasti
//
//
///////////////////////////////////////////////////////////////////////////////

/**
 * ErrMsg
 *
 * This class is used to generate warning and fatal error messages.
 */
class ErrMsg {
    /**
     * Generates a fatal error message.
     * @param lineNum line number for error location
     * @param charNum character number (i.e., column) for error location
     * @param msg associated message for error
     */

    public static boolean fatalErr = false;

    static void fatal(int lineNum, int charNum, String msg) {
        System.err.println(lineNum + ":" + charNum + " ***ERROR*** " + msg);
        fatalErr = true;
    }

    /**
     * Generates a warning message.
     * @param lineNum line number for warning location
     * @param charNum character number (i.e., column) for warning location
     * @param msg associated message for warning
     */
    static void warn(int lineNum, int charNum, String msg) {
        System.err.println(lineNum + ":" + charNum + " ***WARNING*** " + msg);
    }
}
