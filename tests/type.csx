int x;
int y;

struct Loc {
  int m;
  int n;
};

struct Point {
  struct Loc l;
  bool i;
  bool j;
};

void p() {
  cin >> x;
  cin >> y;
  return;
}

void q() {
  cout << x;
  cout << y;
  return;
}


int test(int a, int b) {
  struct Loc loc;
  struct Point po;


  po.i = 1 > 5;
  po.i = x >= y;
  po.j = y < x;
  po.j = y <= x;

  po.i = true;
  po.j = false;

  x++;
  loc.m--;
  
  loc.n = loc.m + 2;
  loc.n = x - y;
  x = loc.m / 2;
  y = x * x + 3;
  
  cout << "New values!";
  p();
  q();

  if (po.i || po.j) {
    po.i = po.i && po.j;
    po.j = !po.i;
  }
 
  while (loc.m == 4) {
    if (x != 4) {
      x++;
    }
    else {
      x--;
    }
  }

  return x;
}


int main() {

  x = test(x, y);
  x = -y;


  return x;
}
