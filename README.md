# README #

### What is this repository for? ###

* CSX Compiler -- Compiler for the CSX language and MIPS architecture.
* Version 1.0

### How do I get set up? ###

* Requires Java CUP and JLex
* Use Makefile to compile compiler into P6.class
* Generate MIPS code with the following command: 
```
#!java

java P6 [input csx file] [output assembly file]
```
