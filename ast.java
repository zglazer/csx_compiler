///////////////////////////////////////////////////////////////////////////////
//
// Title:            ast.java
// Files:            P6.java, CSX.cup, CSX.jlex, ErrMsg.java, typeErrors.csx type.CSX
//                   Codegen.java Type.java
//
// Semester:         CS536 Spring 2014
//
// Author:           Zach Glazer
// Email:            zglazer@wisc.edu
// CS Login:         glazer
// Lecturer's Name:  Hasti
//
//
///////////////////////////////////////////////////////////////////////////////

import java.io.*;
import java.util.*;

// **********************************************************************
// The ASTnode class defines the nodes of the abstract-syntax tree that
// represents a Mini program.
//
// Internal nodes of the tree contain pointers to children, organized
// either in a list (for nodes that may have a variable number of
// children) or as a fixed set of fields.
//
// The nodes for literals and ids contain line and character number
// information; for string literals and identifiers, they also contain a
// string; for integer literals, they also contain an integer value.
//
// Here are all the different kinds of AST nodes and what kinds of children
// they have.  All of these kinds of AST nodes are subclasses of "ASTnode".
// Indentation indicates further subclassing:
//
//     Subclass            Kids
//     --------            ----
//     ProgramNode         DeclListNode
//     DeclListNode        linked list of DeclNode
//     DeclNode:
//       VarDeclNode       TypeNode, IdNode, int
//       FnDeclNode        TypeNode, IdNode, FormalsListNode, FnBodyNode
//       FormalDeclNode    TypeNode, IdNode
//       StructDeclNode    IdNode, DeclListNode
//
//     FormalsListNode     linked list of FormalDeclNode
//     FnBodyNode          DeclListNode, StmtListNode
//     StmtListNode        linked list of StmtNode
//     ExpListNode         linked list of ExpNode
//
//     TypeNode:
//       IntNode           -- none --
//       BoolNode          -- none --
//       VoidNode          -- none --
//       StructNode        IdNode
//
//     StmtNode:
//       AssignStmtNode      AssignNode
//       PostIncStmtNode     ExpNode
//       PostDecStmtNode     ExpNode
//       ReadStmtNode        ExpNode
//       WriteStmtNode       ExpNode
//       IfStmtNode          ExpNode, DeclListNode, StmtListNode
//       IfElseStmtNode      ExpNode, DeclListNode, StmtListNode,
//                                    DeclListNode, StmtListNode
//       WhileStmtNode       ExpNode, DeclListNode, StmtListNode
//       CallStmtNode        CallExpNode
//       ReturnStmtNode      ExpNode
//
//     ExpNode:
//       IntLitNode          -- none --
//       StrLitNode          -- none --
//       TrueNode            -- none --
//       FalseNode           -- none --
//       IdNode              -- none --
//       DotAccessNode       ExpNode, IdNode
//       AssignNode          ExpNode, ExpNode
//       CallExpNode         IdNode, ExpListNode
//       UnaryExpNode        ExpNode
//         UnaryMinusNode
//         NotNode
//       BinaryExpNode       ExpNode ExpNode
//         PlusNode
//         MinusNode
//         TimesNode
//         DivideNode
//         AndNode
//         OrNode
//         EqualsNode
//         NotEqualsNode
//         LessNode
//         GreaterNode
//         LessEqNode
//         GreaterEqNode
//
// Here are the different kinds of AST nodes again, organized according to
// whether they are leaves, internal nodes with linked lists of kids, or
// internal nodes with a fixed number of kids:
//
// (1) Leaf nodes:
//        IntNode,   BoolNode,  VoidNode,  IntLitNode,  StrLitNode,
//        TrueNode,  FalseNode, IdNode
//
// (2) Internal nodes with (possibly empty) linked lists of children:
//        DeclListNode, FormalsListNode, StmtListNode, ExpListNode
//
// (3) Internal nodes with fixed numbers of kids:
//        ProgramNode,     VarDeclNode,     FnDeclNode,     FormalDeclNode,
//        StructDeclNode,  FnBodyNode,      StructNode,     AssignStmtNode,
//        PostIncStmtNode, PostDecStmtNode, ReadStmtNode,   WriteStmtNode
//        IfStmtNode,      IfElseStmtNode,  WhileStmtNode,  CallStmtNode
//        ReturnStmtNode,  DotAccessNode,   AssignExpNode,  CallExpNode,
//        UnaryExpNode,    BinaryExpNode,   UnaryMinusNode, NotNode,
//        PlusNode,        MinusNode,       TimesNode,      DivideNode,
//        AndNode,         OrNode,          EqualsNode,     NotEqualsNode,
//        LessNode,        GreaterNode,     LessEqNode,     GreaterEqNode
//
// **********************************************************************

// **********************************************************************
// ASTnode class (base class for all other kinds of nodes)
// **********************************************************************

abstract class ASTnode {
    // every subclass must provide an unparse operation
    abstract public void unparse(PrintWriter p, int indent);

    // this method can be used by the unparse methods to do indenting
    protected void doIndent(PrintWriter p, int indent) {
        for (int k=0; k<indent; k++) p.print(" ");
    }
    Type t = null;

    public static final int VAR_SIZE = 4;
}

// **********************************************************************
// ProgramNode,  DeclListNode, FormalsListNode, FnBodyNode,
// StmtListNode, ExpListNode
// **********************************************************************

class ProgramNode extends ASTnode {
    public ProgramNode(DeclListNode L) {
        myDeclList = L;
    }

    /**
     * nameAnalysis
     * Creates an empty symbol table for the outermost scope, then processes
     * all of the globals, struct defintions, and functions in the program.
     */
    public void nameAnalysis() {
        SymTable symTab = new SymTable();
        symTab.offset = 1;
        myDeclList.nameAnalysis(symTab);
        if (!hasMain) {
            ErrMsg.fatal(0,0, "No main function.");
        }
    }

    public void typeCheck() {
        myDeclList.typeCheck();
    }

    public void codeGen() {
        myDeclList.codeGen();
    }

    public void unparse(PrintWriter p, int indent) {
        myDeclList.unparse(p, indent);
    }

    // 1 kid
    private DeclListNode myDeclList;
    static boolean hasMain = false;
}

class DeclListNode extends ASTnode {
    public DeclListNode(List<DeclNode> S) {
        myDecls = S;
    }

    /**
     * nameAnalysis
     * Given a symbol table symTab, process all of the decls in the list.
     */
    public void nameAnalysis(SymTable symTab) {
        nameAnalysis(symTab, symTab);
    }

    /**
     * nameAnalysis
     * Given a symbol table symTab and a global symbol table globalTab
     * (for processing struct names in variable decls), process all of the
     * decls in the list.
     */
    public void nameAnalysis(SymTable symTab, SymTable globalTab) {
        for (DeclNode node : myDecls) {
            if (node instanceof VarDeclNode) {
                ((VarDeclNode)node).nameAnalysis(symTab, globalTab);
            } else {
                node.nameAnalysis(symTab);
            }
        }
    }

    public void typeCheck() {
        for (DeclNode node: myDecls) {
            if (node instanceof FnDeclNode) {
                ((FnDeclNode)node).typeCheck();
            }
        }
    }

    public void codeGen() {
        for (DeclNode node: myDecls) {
            node.codeGen();
        }

    }

    public void unparse(PrintWriter p, int indent) {
        Iterator it = myDecls.iterator();
        try {
            while (it.hasNext()) {
                ((DeclNode)it.next()).unparse(p, indent);
            }
        } catch (NoSuchElementException ex) {
            System.err.println("unexpected NoSuchElementException in DeclListNode.print");
            System.exit(-1);
        }
    }

    // list of kids (DeclNodes)
    private List<DeclNode> myDecls;
}

class FormalsListNode extends ASTnode {
    public FormalsListNode(List<FormalDeclNode> S) {
        myFormals = S;
    }

    /**
     * nameAnalysis
     * Given a symbol table symTab, do:
     * for each formal decl in the list
     *     process the formal decl
     *     if there was no error, add type of formal decl to list
     */
    public List<Type> nameAnalysis(SymTable symTab) {
        List<Type> typeList = new LinkedList<Type>();
        for (FormalDeclNode node : myFormals) {
            Sym sym = node.nameAnalysis(symTab);
            if (sym != null) {
                typeList.add(sym.getType());
                sym.setOffset(symTab.offset);
                symTab.offset -= VAR_SIZE;
            }
        }
        return typeList;
    }

    /**
     * Return the number of formals in this list.
     */
    public int length() {
        return myFormals.size();
    }

    /**
     * codeGen:
     * - pushes each formal onto the stack
     */
    public void codeGen() {
        for (FormalDeclNode node: myFormals) {
            node.codeGen();
        }
    }


    public void unparse(PrintWriter p, int indent) {
        Iterator<FormalDeclNode> it = myFormals.iterator();
        if (it.hasNext()) { // if there is at least one element
            it.next().unparse(p, indent);
            while (it.hasNext()) {  // print the rest of the list
                p.print(", ");
                it.next().unparse(p, indent);
            }
        }
    }

    // list of kids (FormalDeclNodes)
    private List<FormalDeclNode> myFormals;
}

class FnBodyNode extends ASTnode {
    public FnBodyNode(DeclListNode declList, StmtListNode stmtList) {
        myDeclList = declList;
        myStmtList = stmtList;
    }

    /**
     * nameAnalysis
     * Given a symbol table symTab, do:
     * - process the declaration list
     * - process the statement list
     */
    public void nameAnalysis(SymTable symTab) {
        myDeclList.nameAnalysis(symTab);
        myStmtList.nameAnalysis(symTab);
    }

    public void typeCheck() {
        myStmtList.definedRetType = definedRetType;
        myStmtList.typeCheck();
        t = definedRetType; // return type as defined in function
    }


    public void codeGen(String fnExitLabel) {
        myStmtList.codeGen(fnExitLabel);
    }

    public void unparse(PrintWriter p, int indent) {
        myDeclList.unparse(p, indent);
        myStmtList.unparse(p, indent);
    }

    // 2 kids
    private DeclListNode myDeclList;
    private StmtListNode myStmtList;
    Type definedRetType = null;
}

class StmtListNode extends ASTnode {
    public StmtListNode(List<StmtNode> S) {
        myStmts = S;
    }

    /**
     * nameAnalysis
     * Given a symbol table symTab, process each statement in the list.
     */
    public void nameAnalysis(SymTable symTab) {
        for (StmtNode node : myStmts) {
            node.nameAnalysis(symTab);
        }
    }

    /**
     * typeCheck
     * - for nested scopes, set defined return type and then typeCheck statements
     * - for ReturnStmtNode check for proper return type
     * - run typeCheck on all other nodes
     */
    public void typeCheck() {
        for (StmtNode node: myStmts) {
            if (node instanceof IfStmtNode) {
                ((IfStmtNode)node).definedRetType = definedRetType;
                node.typeCheck();
            }
            else if (node instanceof IfElseStmtNode) {
                ((IfElseStmtNode)node).definedRetType = definedRetType;
                node.typeCheck();
            }
            else if (node instanceof WhileStmtNode) {
                ((WhileStmtNode)node).definedRetType = definedRetType;
                node.typeCheck();
            }
            else if (node instanceof ReturnStmtNode) {
                // check for correct return type
                node.typeCheck();
                Type ret = node.t;
                if (ret.isVoidType() && !definedRetType.isVoidType()) {
                    ErrMsg.fatal(0, 0, "Missing return value");
                }
                else if (definedRetType.isVoidType() && !ret.isVoidType()) {
                    ReturnStmtNode myRet = (ReturnStmtNode) node;
                    ErrMsg.fatal(myRet.lineNum(), myRet.charNum(), "Return with a value in a void function");
                }
                else if (!definedRetType.isVoidType() && !ret.equals(definedRetType)) {
                    ReturnStmtNode myRet = (ReturnStmtNode) node;
                    ErrMsg.fatal(myRet.lineNum(), myRet.charNum(), "Bad return value");
                }

            }
            else {
                node.typeCheck();
            }
        }
    }

    public void codeGen(String fnExitLabel) {
        for (StmtNode node: myStmts) {
            if (node instanceof ReturnStmtNode) {
                ((ReturnStmtNode)node).fnExitLabel = fnExitLabel;
                node.codeGen();
            }
            else if (node instanceof IfStmtNode) {
                ((IfStmtNode)node).fnExitLabel = fnExitLabel;
                node.codeGen();
            }
            else if (node instanceof IfElseStmtNode) {
                ((IfElseStmtNode)node).fnExitLabel = fnExitLabel;
                node.codeGen();
            }
            else if (node instanceof WhileStmtNode) {
                ((WhileStmtNode)node).fnExitLabel = fnExitLabel;
                node.codeGen();
            }
            else {
                node.codeGen();
            }
        }
    }

    public void unparse(PrintWriter p, int indent) {
        Iterator<StmtNode> it = myStmts.iterator();
        while (it.hasNext()) {
            it.next().unparse(p, indent);
        }
    }

    // list of kids (StmtNodes)
    private List<StmtNode> myStmts;
    Type definedRetType = null;
}

class ExpListNode extends ASTnode {
    public ExpListNode(List<ExpNode> S) {
        myExps = S;
    }

    /**
     * nameAnalysis
     * Given a symbol table symTab, process each exp in the list.
     */
    public void nameAnalysis(SymTable symTab) {
        for (ExpNode node : myExps) {
            node.nameAnalysis(symTab);
        }
    }

    public void typeCheck() {
        for (ExpNode node : myExps) {
            node.typeCheck();
        }
    }

    public List<ExpNode> getExps() {
        return myExps;
    }

    public int size() {
        return myExps.size();
    }

    public void codeGen() {
        for (ExpNode node: myExps) {
            node.codeGen();
        }
    }

    public void unparse(PrintWriter p, int indent) {
        Iterator<ExpNode> it = myExps.iterator();
        if (it.hasNext()) { // if there is at least one element
            it.next().unparse(p, indent);
            while (it.hasNext()) {  // print the rest of the list
                p.print(", ");
                it.next().unparse(p, indent);
            }
        }
    }

    // list of kids (ExpNodes)
    private List<ExpNode> myExps;
}

// **********************************************************************
// DeclNode and its subclasses
// **********************************************************************

abstract class DeclNode extends ASTnode {
    /**
     * Note: a formal decl needs to return a sym
     */
    abstract public Sym nameAnalysis(SymTable symTab);
    abstract public void codeGen();
}

class VarDeclNode extends DeclNode {
    public VarDeclNode(TypeNode type, IdNode id, int size) {
        myType = type;
        myId = id;
        mySize = size;
    }

    /**
     * nameAnalysis (overloaded)
     * Given a symbol table symTab, do:
     * if this name is declared void, then error
     * else if the declaration is of a struct type,
     *     lookup type name (globally)
     *     if type name doesn't exist, then error
     * if no errors so far,
     *     if name has already been declared in this scope, then error
     *     else add name to local symbol table
     *
     * symTab is local symbol table (say, for struct field decls)
     * globalTab is global symbol table (for struct type names)
     * symTab and globalTab can be the same
     */
    public Sym nameAnalysis(SymTable symTab) {
        return nameAnalysis(symTab, symTab);
    }

    /**
     * nameAnalysis
     * - run name analysis on variable declarations (can be regular variable or a struct)
     *
     */
    public Sym nameAnalysis(SymTable symTab, SymTable globalTab) {
        boolean badDecl = false;
        String name = myId.name();
        Sym sym = null;
        IdNode structId = null;

        if (myType instanceof VoidNode) {  // check for void type
            ErrMsg.fatal(myId.lineNum(), myId.charNum(),
                         "Non-function declared void");
            badDecl = true;
        }

        else if (myType instanceof StructNode) {
            structId = ((StructNode)myType).idNode();
            sym = globalTab.lookupGlobal(structId.name());

            // if the name for the struct type is not found,
            // or is not a struct type
            if (sym == null || !(sym instanceof StructDefSym)) {
                ErrMsg.fatal(structId.lineNum(), structId.charNum(),
                             "Invalid name of struct type");
                badDecl = true;
            }
            else {
                structId.link(sym);
            }
        }

        if (symTab.lookupLocal(name) != null) {
            ErrMsg.fatal(myId.lineNum(), myId.charNum(),
                         "Multiply declared identifier");
            badDecl = true;
        }

        if (!badDecl) {  // insert into symbol table
            try {
                if (myType instanceof StructNode) {
                    sym = new StructSym(structId);
                }
                else {
                    sym = new Sym(myType.type());
                    sym.setOffset(symTab.offset);
                    if (symTab.offset < 1) {
                        symTab.offset -= VAR_SIZE;
                    }
                }
                symTab.addDecl(name, sym);
                myId.link(sym);
            } catch (DuplicateSymException ex) {
                System.err.println("Unexpected DuplicateSymException " +
                                   " in VarDeclNode.nameAnalysis");
                System.exit(-1);
            } catch (EmptySymTableException ex) {
                System.err.println("Unexpected EmptySymTableException " +
                                   " in VarDeclNode.nameAnalysis");
                System.exit(-1);
            }
        }

        return sym;
    }

    public void codeGen() {
        // global variable declaration
        if (myId.sym().isGlobal()) {
            Codegen.generate(".data");
            Codegen.generate(".align 2");
            Codegen.generateLabeled("_" + myId.name(), ".space 4", "", "");
        }
    }

    public void unparse(PrintWriter p, int indent) {
        doIndent(p, indent);
        myType.unparse(p, 0);
        p.print(" ");
        p.print(myId.name());
        p.print(" [" + myId.sym().getOffset() + "]");
        p.println(";");
    }

    // 3 kids
    private TypeNode myType;
    private IdNode myId;
    private int mySize;  // use value NOT_STRUCT if this is not a struct type

    public static int NOT_STRUCT = -1;
}

class FnDeclNode extends DeclNode {
    public FnDeclNode(TypeNode type,
                      IdNode id,
                      FormalsListNode formalList,
                      FnBodyNode body) {
        myType = type;
        myId = id;
        myFormalsList = formalList;
        myBody = body;
    }

    /**
     * nameAnalysis
     * Given a symbol table symTab, do:
     * if this name has already been declared in this scope, then error
     * else add name to local symbol table
     * in any case, do the following:
     *     enter new scope
     *     process the formals and calculate offsets
     *     if this function is not multiply declared,
     *         update symbol table entry with types of formals
     *     process the body of the function
     *     exit scope
     */
    public Sym nameAnalysis(SymTable symTab) {
        String name = myId.name();
        FnSym sym = null;

        if (symTab.lookupLocal(name) != null) {
            ErrMsg.fatal(myId.lineNum(), myId.charNum(),
                         "Multiply declared identifier");
        }

        else { // add function name to local symbol table
            try {
                sym = new FnSym(myType.type(), myFormalsList.length());
                symTab.addDecl(name, sym);
                myId.link(sym);
                if (myId.name().equalsIgnoreCase("main")) {
                    ProgramNode.hasMain = true;
                }
            } catch (DuplicateSymException ex) {
                System.err.println("Unexpected DuplicateSymException " +
                                   " in FnDeclNode.nameAnalysis");
                System.exit(-1);
            } catch (EmptySymTableException ex) {
                System.err.println("Unexpected EmptySymTableException " +
                                   " in FnDeclNode.nameAnalysis");
                System.exit(-1);
            }
        }

        symTab.addScope();  // add a new scope for locals and params
        symTab.offset = 0; // start offset at 0

        // process the formals
        List<Type> typeList = myFormalsList.nameAnalysis(symTab);
        if (sym != null) {
            sym.addFormals(typeList);
        }
        paramSize = VAR_SIZE * typeList.size(); // size of parameters

        symTab.offset -= 2 * VAR_SIZE; // account for CL and RA
        myBody.nameAnalysis(symTab); // process the function body

        try {
            symTab.removeScope();  // exit scope
        } catch (EmptySymTableException ex) {
            System.err.println("Unexpected EmptySymTableException " +
                               " in FnDeclNode.nameAnalysis");
            System.exit(-1);
        }

        sym.setOffset(symTab.offset); // save total space for vars in function;
        symTab.offset = 1;
        return null;
    }

    public void typeCheck() {
        t = myType.type();
        myBody.definedRetType = t;
        myBody.typeCheck();
    }

    public void codeGen() {

        /**
         * FUNCTION PROLOGUE
         */

        String fnExit = "_" + myId.name() + "_Exit"; // label for function exit

        // check if function is main and generate labels
        if (myId.name().equalsIgnoreCase("main")) {
            Codegen.generate(".text");
            Codegen.generate(".globl main");
            Codegen.genLabel("main");
            Codegen.genLabel("__start");
        }
        else {
            Codegen.generate(".text");
            Codegen.genLabel("_" + myId.name());
        }

        // push return address
        Codegen.genPush(Codegen.RA);

        // push control link
        Codegen.genPush(Codegen.FP);

        // set new frame pointer
        // FP = SP + 8 + sizeof(params)
        Codegen.generate("addu", Codegen.FP, Codegen.SP, 8 + paramSize);

        // push space for local vars
        // SP = SP - sizeof(local vars)
        int locals = -(myId.sym().getOffset());
        if (locals > 0) {
            Codegen.generate("subu", Codegen.SP, Codegen.SP, locals);
        }

        // code generation for body of the function
        myBody.codeGen(fnExit);

        /**
         * FUNCTION EPILOGUE
         */

        Codegen.genLabel(fnExit);

        // load return address
        Codegen.generateIndexed("lw", Codegen.RA, Codegen.FP, -paramSize);

        // save current FP
        Codegen.generate("move", Codegen.T0, Codegen.FP);

        // restore old FP from control link
        Codegen.generateIndexed("lw", Codegen.FP, Codegen.FP, -(paramSize) - 4);

        // restore old SP
        Codegen.generate("move", Codegen.SP, Codegen.T0);

        // return
        if (myId.name().equalsIgnoreCase("main")) {
            Codegen.generate("li", Codegen.V0, 10);
            Codegen.generate("syscall");
        }
        else {
            Codegen.generate("jr", Codegen.RA);
        }
    }

    public void unparse(PrintWriter p, int indent) {
        doIndent(p, indent);
        myType.unparse(p, 0);
        p.print(" ");
        p.print(myId.name());
        p.print("(");
        myFormalsList.unparse(p, 0);
        p.println(") {");
        myBody.unparse(p, indent+4);
        p.println("}\n");
    }

    // 4 kids
    private TypeNode myType;
    private IdNode myId;
    private FormalsListNode myFormalsList;
    private FnBodyNode myBody;
    private int paramSize;
}

class FormalDeclNode extends DeclNode {
    public FormalDeclNode(TypeNode type, IdNode id) {
        myType = type;
        myId = id;
    }

    /**
     * nameAnalysis
     * Given a symbol table symTab, do:
     * if this formal is declared void, then error
     * else if this formal is already in the local symble table,
     *     then issue multiply declared error message and return null
     * else add a new entry to the symbol table and return that Sym
     */
    public Sym nameAnalysis(SymTable symTab) {
        String name = myId.name();
        boolean badDecl = false;
        Sym sym = null;

        if (myType instanceof VoidNode) {
            ErrMsg.fatal(myId.lineNum(), myId.charNum(),
                         "Non-function declared void");
            badDecl = true;
        }

        if (symTab.lookupLocal(name) != null) {
            ErrMsg.fatal(myId.lineNum(), myId.charNum(),
                         "Multiply declared identifier");
            badDecl = true;
        }

        if (!badDecl) {  // insert into symbol table
            try {
                sym = new Sym(myType.type());
                symTab.addDecl(name, sym);
                myId.link(sym);
            } catch (DuplicateSymException ex) {
                System.err.println("Unexpected DuplicateSymException " +
                                   " in VarDeclNode.nameAnalysis");
                System.exit(-1);
            } catch (EmptySymTableException ex) {
                System.err.println("Unexpected EmptySymTableException " +
                                   " in VarDeclNode.nameAnalysis");
                System.exit(-1);
            }
        }

        return sym;
    }

    public void unparse(PrintWriter p, int indent) {
        myType.unparse(p, 0);
        p.print(" ");
        p.print(myId.name());
        p.print(" [" + myId.sym().getOffset() + "]");
    }

    public void codeGen() {
        myId.codeGen();
    }

    // 2 kids
    private TypeNode myType;
    private IdNode myId;
}

class StructDeclNode extends DeclNode {
    public StructDeclNode(IdNode id, DeclListNode declList) {
        myId = id;
        myDeclList = declList;
    }

    /**
     * nameAnalysis
     * Given a symbol table symTab, do:
     * if this name is already in the symbol table,
     *     then multiply declared error (don't add to symbol table)
     * create a new symbol table for this struct definition
     * process the decl list
     * if no errors
     *     add a new entry to symbol table for this struct
     */
    public Sym nameAnalysis(SymTable symTab) {
        String name = myId.name();
        boolean badDecl = false;

        if (symTab.lookupLocal(name) != null) {
            ErrMsg.fatal(myId.lineNum(), myId.charNum(),
                         "Multiply declared identifier");
            badDecl = true;
        }

        SymTable structSymTab = new SymTable();

        // process the fields of the struct
        myDeclList.nameAnalysis(structSymTab, symTab);

        if (!badDecl) {
            try {   // add entry to symbol table
                StructDefSym sym = new StructDefSym(structSymTab);
                symTab.addDecl(name, sym);
                myId.link(sym);
            } catch (DuplicateSymException ex) {
                System.err.println("Unexpected DuplicateSymException " +
                                   " in StructDeclNode.nameAnalysis");
                System.exit(-1);
            } catch (EmptySymTableException ex) {
                System.err.println("Unexpected EmptySymTableException " +
                                   " in StructDeclNode.nameAnalysis");
                System.exit(-1);
            }
        }

        return null;
    }

    public void typeCheck() {
        t = new StructDefType();
    }

    public void codeGen() {
        // EMPTY
    }

    public void unparse(PrintWriter p, int indent) {
        doIndent(p, indent);
        p.print("struct ");
        p.print(myId.name());
        p.println("{");
        myDeclList.unparse(p, indent+4);
        doIndent(p, indent);
        p.println("};\n");

    }

    // 2 kids
    private IdNode myId;
    private DeclListNode myDeclList;
}

// **********************************************************************
// TypeNode and its Subclasses
// **********************************************************************

abstract class TypeNode extends ASTnode {
    /* all subclasses must provide a type method */
    abstract public Type type(); // return Type of TypeNode

    public void typeCheck() { t = type(); }
}

class IntNode extends TypeNode {
    public IntNode() {
    }

    /**
     * type
     */
    public Type type() {
        return new IntType();
    }

    public void unparse(PrintWriter p, int indent) {
        p.print("int");
    }
}

class BoolNode extends TypeNode {
    public BoolNode() {
    }

    /**
     * type
     */
    public Type type() {
        return new BoolType();
    }

    public void unparse(PrintWriter p, int indent) {
        p.print("bool");
    }
}

class VoidNode extends TypeNode {
    public VoidNode() {
    }

    /**
     * type
     */
    public Type type() {
        return new VoidType();
    }

    public void unparse(PrintWriter p, int indent) {
        p.print("void");
    }
}

class StructNode extends TypeNode {
    public StructNode(IdNode id) {
        myId = id;
    }

    public IdNode idNode() {
        return myId;
    }

    /**
     * type
     */
    public Type type() {
        return new StructType(myId);
    }

    public void unparse(PrintWriter p, int indent) {
        p.print("struct ");
        p.print(myId.name());
    }

    // 1 kid
    private IdNode myId;
}

// **********************************************************************
// StmtNode and its subclasses
// **********************************************************************

abstract class StmtNode extends ASTnode {
    abstract public void nameAnalysis(SymTable symTab);
    abstract public void typeCheck();
    abstract public void codeGen();
}

class AssignStmtNode extends StmtNode {
    public AssignStmtNode(AssignNode assign) {
        myAssign = assign;
    }

    /**
     * nameAnalysis
     * Given a symbol table symTab, perform name analysis on this node's child
     */
    public void nameAnalysis(SymTable symTab) {
        myAssign.nameAnalysis(symTab);
    }

    public void typeCheck() {
        myAssign.typeCheck();
        t = myAssign.t;
    }

    public void codeGen() {
        myAssign.codeGen();
        Codegen.genPop(Codegen.T0);
    }

    public void unparse(PrintWriter p, int indent) {
        doIndent(p, indent);
        myAssign.unparse(p, -1); // no parentheses
        p.println(";");
    }

    // 1 kid
    private AssignNode myAssign;
}

class PostIncStmtNode extends StmtNode {
    public PostIncStmtNode(ExpNode exp) {
        myExp = exp;
    }

    /**
     * nameAnalysis
     * Given a symbol table symTab, perform name analysis on this node's child
     */
    public void nameAnalysis(SymTable symTab) {
        myExp.nameAnalysis(symTab);
    }

    public void typeCheck() {
        myExp.typeCheck();
        if (myExp.t.isErrorType()) {
            t = new ErrorType();
            return;
        }
        if (!myExp.t.isIntType()) {
            ErrMsg.fatal(myExp.lineNum(), myExp.charNum(), "Arithmetic operator " +
                    "applied to non-numeric operand");
            t = new ErrorType();
        }
        else {
            t = myExp.t;
        }
    }

    public void codeGen() {
        if (myExp instanceof IdNode) {
            IdNode myId = (IdNode) myExp;
            myId.codeGen();
            Codegen.genPop(Codegen.T0);
            Codegen.generate("addi", Codegen.T0, Codegen.T0, 1);

            if (myId.sym().isGlobal()) {
                Codegen.generateWithComment("sw", "POST INC", Codegen.T0, "_" + myId.name());
            } else {
                Codegen.generateIndexed("sw", Codegen.T0, Codegen.FP, myId.sym().getOffset(), "POST INC");
            }
        }
    }

    public void unparse(PrintWriter p, int indent) {
        doIndent(p, indent);
        myExp.unparse(p, 0);
        p.println("++;");
    }

    // 1 kid
    private ExpNode myExp;
}

class PostDecStmtNode extends StmtNode {
    public PostDecStmtNode(ExpNode exp) {
        myExp = exp;
    }

    /**
     * nameAnalysis
     * Given a symbol table symTab, perform name analysis on this node's child
     */
    public void nameAnalysis(SymTable symTab) {
        myExp.nameAnalysis(symTab);
    }

    public void typeCheck() {
        myExp.typeCheck();

        if (myExp.t.isErrorType()) {
            t = new ErrorType();
            return;
        }

        if (!myExp.t.isIntType()) {
            ErrMsg.fatal(myExp.lineNum(), myExp.charNum(), "Arithmetic operator " +
                    "applied to non-numeric operand");
            t = new ErrorType();
        }
        else {
            t = myExp.t;
        }
    }

    public void codeGen() {
        if (myExp instanceof IdNode) {
            IdNode myId = (IdNode) myExp;
            myId.codeGen();
            Codegen.genPop(Codegen.T0);
            Codegen.generate("addi", Codegen.T0, Codegen.T0, -1);

            if (myId.sym().isGlobal()) {
                Codegen.generateWithComment("sw", "POST DEC", Codegen.T0, "_" + myId.name());
            } else {
                Codegen.generateIndexed("sw", Codegen.T0, Codegen.FP, myId.sym().getOffset(), "POST DEC");
            }
        }
    }

    public void unparse(PrintWriter p, int indent) {
        doIndent(p, indent);
        myExp.unparse(p, 0);
        p.println("--;");
    }

    // 1 kid
    private ExpNode myExp;
}

class ReadStmtNode extends StmtNode {
    public ReadStmtNode(ExpNode e) {
        myExp = e;
    }

    /**
     * nameAnalysis
     * Given a symbol table symTab, perform name analysis on this node's child
     */
    public void nameAnalysis(SymTable symTab) {
        myExp.nameAnalysis(symTab);
    }

    public void typeCheck() {
        myExp.typeCheck();

        if (myExp.t.isErrorType()) {
            return;
        }
        if (myExp.t.isFnType()) {
            IdNode myId = (IdNode) myExp;
            ErrMsg.fatal(myId.lineNum(), myId.charNum(), "Attempt to read a function");
        }
        else if (myExp.t.isStructDefType()) {
            IdNode myId = (IdNode) myExp;
            ErrMsg.fatal(myId.lineNum(), myId.charNum(), "Attempt to read a struct name");
        }
        else if (myExp.t.isStructType()) {
            IdNode myId = (IdNode) myExp;
            ErrMsg.fatal(myId.lineNum(), myId.charNum(), "Attempt to read a struct variable");
        }
    }

    public void codeGen() {
        // read in value, store at location
        if (myExp instanceof IdNode) {
            IdNode myId = (IdNode) myExp;
            // read system call
            Codegen.generate("li", Codegen.V0, 5);
            Codegen.generate("syscall");

            // result stored in $v0
            if (myId.sym().isGlobal()) {
                Codegen.generateWithComment("sw", "READ", Codegen.V0, "_" + myId.name());
            }
            else {
                Codegen.generateIndexed("sw", Codegen.V0, Codegen.FP, myId.sym().getOffset(), "READ");
            }
        }
        else {
            ErrMsg.fatal(myExp.lineNum(), myExp.charNum(), "Attempt to read a non Id.");
        }
    }

    public void unparse(PrintWriter p, int indent) {
        doIndent(p, indent);
        p.print("cin >> ");
        myExp.unparse(p, 0);
        p.println(";");
    }

    // 1 kid (actually can only be an IdNode or an ArrayExpNode)
    private ExpNode myExp;
}

class WriteStmtNode extends StmtNode {
    public WriteStmtNode(ExpNode exp) {
        myExp = exp;
    }

    /**
     * nameAnalysis
     * Given a symbol table symTab, perform name analysis on this node's child
     */
    public void nameAnalysis(SymTable symTab) {
        myExp.nameAnalysis(symTab);
    }

    public void typeCheck() {
        myExp.typeCheck();

        if (myExp.t.isErrorType()) {
            return;
        }
        if (myExp.t.isFnType()) {
            IdNode myId = (IdNode) myExp;
            ErrMsg.fatal(myId.lineNum(), myId.charNum(), "Attempt to write a function");
        }
        else if (myExp.t.isStructDefType()) {
            IdNode myId = (IdNode) myExp;
            ErrMsg.fatal(myId.lineNum(), myId.charNum(), "Attempt to write a struct name");
        }
        else if (myExp.t.isStructType()) {
            IdNode myId = (IdNode) myExp;
            ErrMsg.fatal(myId.lineNum(), myId.charNum(), "Attempt to write a struct variable");
        }
        else if (myExp.t.isVoidType()) {
            ErrMsg.fatal(myExp.lineNum(), myExp.charNum(), "Attempt to write void");
        }

        myType = myExp.t;
    }

    public void codeGen() {
        if (myType.isBoolType() || myType.isIntType()) {
            myExp.codeGen(); // puts result on stack
            Codegen.genPop(Codegen.A0);
            Codegen.generate("li", Codegen.V0, 1);
            Codegen.generate("syscall");
        }

        else if (myType.isStringType()) {
            // get label on the stack
            myExp.codeGen();

            // push args
            Codegen.genPop(Codegen.A0);
            Codegen.generate("li", Codegen.V0, 4);

            // syscall
            Codegen.generate("syscall");
        }

        else {
            ErrMsg.fatal(myExp.lineNum(), myExp.charNum(), "Attempt to write non int, bool, or string");
        }
    }

    public void unparse(PrintWriter p, int indent) {
        doIndent(p, indent);
        p.print("cout << ");
        myExp.unparse(p, 0);
        p.println(";");
    }

    // 1 kid
    private ExpNode myExp;
    private Type myType;
}

class IfStmtNode extends StmtNode {
    public IfStmtNode(ExpNode exp, DeclListNode dlist, StmtListNode slist) {
        myDeclList = dlist;
        myExp = exp;
        myStmtList = slist;
    }

    /**
     * nameAnalysis
     * Given a symbol table symTab, do:
     * - process the condition
     * - enter a new scope
     * - process the decls and stmts
     * - exit the scope
     */
    public void nameAnalysis(SymTable symTab) {
        myExp.nameAnalysis(symTab);
        symTab.addScope();
        myDeclList.nameAnalysis(symTab);
        myStmtList.nameAnalysis(symTab);
        try {
            symTab.removeScope();
        } catch (EmptySymTableException ex) {
            System.err.println("Unexpected EmptySymTableException " +
                               " in IfStmtNode.nameAnalysis");
            System.exit(-1);
        }
    }

    public void typeCheck() {
        myExp.typeCheck();
        if (!myExp.t.isBoolType()) {
            ErrMsg.fatal(myExp.lineNum(), myExp.charNum(), "Non-bool expression used as an if condition");
        }

        myStmtList.definedRetType = definedRetType;
        myStmtList.typeCheck();
    }

    public void codeGen() {
        // evaluate boolean
        String falseLabel = Codegen.nextLabel();
        myExp.codeGen();
        Codegen.genPop(Codegen.T0);

        // branch if false
        Codegen.generate("beq", Codegen.T0, Codegen.FALSE, falseLabel);

        // if true continue
        myStmtList.codeGen(fnExitLabel);

        // if false jump over branch
        Codegen.genLabel(falseLabel);
    }

    public void unparse(PrintWriter p, int indent) {
        doIndent(p, indent);
        p.print("if (");
        myExp.unparse(p, 0);
        p.println(") {");
        myDeclList.unparse(p, indent+4);
        myStmtList.unparse(p, indent+4);
        doIndent(p, indent);
        p.println("}");
    }

    // 3 kids
    private ExpNode myExp;
    private DeclListNode myDeclList;
    private StmtListNode myStmtList;
    Type definedRetType = null;
    String fnExitLabel;
}

class IfElseStmtNode extends StmtNode {
    public IfElseStmtNode(ExpNode exp, DeclListNode dlist1,
                          StmtListNode slist1, DeclListNode dlist2,
                          StmtListNode slist2) {
        myExp = exp;
        myThenDeclList = dlist1;
        myThenStmtList = slist1;
        myElseDeclList = dlist2;
        myElseStmtList = slist2;
    }

    /**
     * nameAnalysis
     * Given a symbol table symTab, do:
     * - process the condition
     * - enter a new scope
     * - process the decls and stmts of then
     * - exit the scope
     * - enter a new scope
     * - process the decls and stmts of else
     * - exit the scope
     */
    public void nameAnalysis(SymTable symTab) {
        myExp.nameAnalysis(symTab);
        symTab.addScope();
        myThenDeclList.nameAnalysis(symTab);
        myThenStmtList.nameAnalysis(symTab);
        try {
            symTab.removeScope();
        } catch (EmptySymTableException ex) {
            System.err.println("Unexpected EmptySymTableException " +
                               " in IfStmtNode.nameAnalysis");
            System.exit(-1);
        }
        symTab.addScope();
        myElseDeclList.nameAnalysis(symTab);
        myElseStmtList.nameAnalysis(symTab);
        try {
            symTab.removeScope();
        } catch (EmptySymTableException ex) {
            System.err.println("Unexpected EmptySymTableException " +
                               " in IfStmtNode.nameAnalysis");
            System.exit(-1);
        }
    }

    public void typeCheck() {
        myExp.typeCheck();
        if (!myExp.t.isBoolType()) {
            ErrMsg.fatal(myExp.lineNum(), myExp.charNum(), "Non-bool expression used as an if condition");
        }

        myThenStmtList.definedRetType = definedRetType;
        myElseStmtList.definedRetType = definedRetType;

        myThenStmtList.typeCheck();
        myElseStmtList.typeCheck();
    }

    public void codeGen() {
        // evaluate boolean
        String falseLabel = Codegen.nextLabel();
        String endLabel = Codegen.nextLabel();

        myExp.codeGen();
        Codegen.genPop(Codegen.T0);

        // branch if false
        Codegen.generate("beq", Codegen.T0, Codegen.FALSE, falseLabel);

        // if true continue
        myThenStmtList.codeGen(fnExitLabel);
        Codegen.generate("b", endLabel);

        // if false jump over branch
        Codegen.genLabel(falseLabel);
        myElseStmtList.codeGen(fnExitLabel);

        Codegen.genLabel(endLabel);
    }

    public void unparse(PrintWriter p, int indent) {
        doIndent(p, indent);
        p.print("if (");
        myExp.unparse(p, 0);
        p.println(") {");
        myThenDeclList.unparse(p, indent+4);
        myThenStmtList.unparse(p, indent+4);
        doIndent(p, indent);
        p.println("}");
        doIndent(p, indent);
        p.println("else {");
        myElseDeclList.unparse(p, indent+4);
        myElseStmtList.unparse(p, indent+4);
        doIndent(p, indent);
        p.println("}");
    }

    // 5 kids
    private ExpNode myExp;
    private DeclListNode myThenDeclList;
    private StmtListNode myThenStmtList;
    private StmtListNode myElseStmtList;
    private DeclListNode myElseDeclList;
    Type definedRetType = null;
    String fnExitLabel;
}

class WhileStmtNode extends StmtNode {
    public WhileStmtNode(ExpNode exp, DeclListNode dlist, StmtListNode slist) {
        myExp = exp;
        myDeclList = dlist;
        myStmtList = slist;
    }

    /**
     * nameAnalysis
     * Given a symbol table symTab, do:
     * - process the condition
     * - enter a new scope
     * - process the decls and stmts
     * - exit the scope
     */
    public void nameAnalysis(SymTable symTab) {
        myExp.nameAnalysis(symTab);
        symTab.addScope();
        myDeclList.nameAnalysis(symTab);
        myStmtList.nameAnalysis(symTab);
        try {
            symTab.removeScope();
        } catch (EmptySymTableException ex) {
            System.err.println("Unexpected EmptySymTableException " +
                               " in IfStmtNode.nameAnalysis");
            System.exit(-1);
        }
    }

    public void typeCheck() {
        myExp.typeCheck();
        if (!myExp.t.isBoolType()) {
            ErrMsg.fatal(myExp.lineNum(), myExp.charNum(), "Non-bool expression used as a while condition");
        }
        myStmtList.definedRetType = definedRetType;
        myStmtList.typeCheck();
    }

    public void codeGen() {
        // evaluate boolean
        String loopLabel = Codegen.nextLabel();
        String endLabel = Codegen.nextLabel();

        Codegen.genLabel(loopLabel);
        myExp.codeGen();
        Codegen.genPop(Codegen.T0);

        // branch if false
        Codegen.generate("beq", Codegen.T0, Codegen.FALSE, endLabel);

        // if true continue
        myStmtList.codeGen(fnExitLabel);
        Codegen.generate("b", loopLabel);

        Codegen.genLabel(endLabel);
    }

    public void unparse(PrintWriter p, int indent) {
        doIndent(p, indent);
        p.print("while (");
        myExp.unparse(p, 0);
        p.println(") {");
        myDeclList.unparse(p, indent+4);
        myStmtList.unparse(p, indent+4);
        doIndent(p, indent);
        p.println("}");
    }

    // 3 kids
    private ExpNode myExp;
    private DeclListNode myDeclList;
    private StmtListNode myStmtList;
    Type definedRetType = null;
    String fnExitLabel;
}

class CallStmtNode extends StmtNode {
    public CallStmtNode(CallExpNode call) {
        myCall = call;
    }

    /**
     * nameAnalysis
     * Given a symbol table symTab, perform name analysis on this node's child
     */
    public void nameAnalysis(SymTable symTab) {
        myCall.nameAnalysis(symTab);
    }

    public void typeCheck() {
        myCall.typeCheck();
        t = myCall.t;
    }

    public void codeGen() {
        myCall.codeGen();
        Codegen.genPop(Codegen.T0);
    }

    public void unparse(PrintWriter p, int indent) {
        doIndent(p, indent);
        myCall.unparse(p, indent);
        p.println(";");
    }

    // 1 kid
    private CallExpNode myCall;
}

class ReturnStmtNode extends StmtNode {
    public ReturnStmtNode(ExpNode exp) {
        myExp = exp;
    }

    /**
     * nameAnalysis
     * Given a symbol table symTab, perform name analysis on this node's child,
     * if it has one
     */
    public void nameAnalysis(SymTable symTab) {
        if (myExp != null) {
            myExp.nameAnalysis(symTab);
        }
    }

    public void typeCheck() {
        if (myExp != null) {
            myExp.typeCheck();
            t = myExp.t;
        }
        else {
            t = new VoidType();
        }
    }

    public int lineNum() {
        return myExp.lineNum();
    }

    public int charNum() {
        return myExp.charNum();
    }

    public void codeGen(String fnExitLabel) {
        if (myExp != null) {
            myExp.codeGen();

            // put return value into $v0
            Codegen.genPop(Codegen.V0);
        }

        // branch to function exit (for epilogue)
        Codegen.generate("b", fnExitLabel);
    }

    public void codeGen() {
        codeGen(fnExitLabel);
    }

    public void unparse(PrintWriter p, int indent) {
        doIndent(p, indent);
        p.print("return");
        if (myExp != null) {
            p.print(" ");
            myExp.unparse(p, 0);
        }
        p.println(";");
    }

    // 1 kid
    private ExpNode myExp; // possibly null
    String fnExitLabel;
}

// **********************************************************************
// ExpNode and its subclasses
// **********************************************************************

abstract class ExpNode extends ASTnode {
    /**
     * Default version for nodes with no names
     */
    public void nameAnalysis(SymTable symTab) { }

    abstract void typeCheck();
    abstract void codeGen();

    abstract int lineNum();
    abstract int charNum();
}

class IntLitNode extends ExpNode {
    public IntLitNode(int lineNum, int charNum, int intVal) {
        myLineNum = lineNum;
        myCharNum = charNum;
        myIntVal = intVal;
    }

    public void typeCheck() {
        t = new IntType();
    }

    public int lineNum() {
        return myLineNum;
    }

    public int charNum() {
        return myCharNum;
    }

    public void codeGen() {
        Codegen.generate("li", Codegen.T0, myIntVal);
        Codegen.genPush(Codegen.T0);
    }

    public void unparse(PrintWriter p, int indent) {
        p.print(myIntVal);
    }

    private int myLineNum;
    private int myCharNum;
    private int myIntVal;
}

class StringLitNode extends ExpNode {
    public StringLitNode(int lineNum, int charNum, String strVal) {
        myLineNum = lineNum;
        myCharNum = charNum;
        myStrVal = strVal;
    }

    public void typeCheck() {
        t = new StringType();
    }

    public int lineNum() {
        return myLineNum;
    }

    public int charNum() {
        return myCharNum;
    }

    public void codeGen() {
        Codegen.generate(".data");
        String newLabel = Codegen.nextLabel();
        Codegen.generateLabeled(newLabel, ".asciiz ", "", myStrVal);
        Codegen.generate(".text");

        Codegen.generate("la", Codegen.T0, newLabel);
        Codegen.genPush(Codegen.T0);
    }


    public void unparse(PrintWriter p, int indent) {
        p.print(myStrVal);
    }

    private int myLineNum;
    private int myCharNum;
    private String myStrVal;
}

class TrueNode extends ExpNode {
    public TrueNode(int lineNum, int charNum) {
        myLineNum = lineNum;
        myCharNum = charNum;
    }

    public void typeCheck() {
        t = new BoolType();
    }

    public int lineNum() {
        return myLineNum;
    }

    public int charNum() {
        return myCharNum;
    }

    public void codeGen() {
        Codegen.generate("li", Codegen.T0, Codegen.TRUE);
        Codegen.genPush(Codegen.T0);
    }

    public void unparse(PrintWriter p, int indent) {
        p.print("true");
    }

    private int myLineNum;
    private int myCharNum;
}

class FalseNode extends ExpNode {
    public FalseNode(int lineNum, int charNum) {
        myLineNum = lineNum;
        myCharNum = charNum;
    }

    public void typeCheck() {
        t = new BoolType();
    }

    public int lineNum() {
        return myLineNum;
    }

    public int charNum() {
        return myCharNum;
    }

    public void codeGen() {
        Codegen.generate("li", Codegen.T0, Codegen.FALSE);
        Codegen.genPush(Codegen.T0);
    }

    public void unparse(PrintWriter p, int indent) {
        p.print("false");
    }

    private int myLineNum;
    private int myCharNum;
}

class IdNode extends ExpNode {
    public IdNode(int lineNum, int charNum, String strVal) {
        myLineNum = lineNum;
        myCharNum = charNum;
        myStrVal = strVal;
    }

    /**
     * Link the given symbol to this ID.
     */
    public void link(Sym sym) {
        mySym = sym;
    }

    /**
     * Return the name of this ID.
     */
    public String name() {
        return myStrVal;
    }

    /**
     * Return the symbol associated with this ID.
     */
    public Sym sym() {
        return mySym;
    }

    /**
     * Return the line number for this ID.
     */
    public int lineNum() {
        return myLineNum;
    }

    /**
     * Return the char number for this ID.
     */
    public int charNum() {
        return myCharNum;
    }

    /**
     * nameAnalysis
     * Given a symbol table symTab, do:
     * - check for use of undeclared name
     * - if ok, link to symbol table entry
     */
    public void nameAnalysis(SymTable symTab) {
        Sym sym = symTab.lookupGlobal(myStrVal);
        if (sym == null) {
            ErrMsg.fatal(myLineNum, myCharNum, "Undeclared identifier");
        } else {
            link(sym);
        }
    }

    public void typeCheck() {
        t = mySym.getType();
    }

    /**
     * codeGen
     * Generate code for IDs used in expressions
     * - should only be called for variables
     * - pushes ID value onto stack
     */
    public void codeGen() {
        if (mySym.getType().isIntType() || mySym.getType().isBoolType()) {
            if (mySym.isGlobal()) {

                // get value from label _myName
                Codegen.generate("lw", Codegen.T0, "_" + myStrVal);
                Codegen.genPush(Codegen.T0);

            } else {
                // local variable
                // Id's value is in FP - params - CL - RA
                int offset = mySym.getOffset();

                // load value at offset into register T0
                // push T0 onto stack
                Codegen.generateIndexed("lw", Codegen.T0, Codegen.FP, offset, "USE OF " + myStrVal);
                Codegen.genPush(Codegen.T0);
            }
        }

        // function call -- this should actually be handled in an expression
        else if (mySym.getType().isFnType()) {

        }

        // error, unsupported type
        else {
            ErrMsg.fatal(lineNum(), charNum(), "Unsupported Id type");
        }
    }

    public void generateVarLoc() {
        if (mySym.isGlobal()) {
            Codegen.generate("la", Codegen.T1, "_" + myStrVal);
            Codegen.genPush(Codegen.T1);
        }

        else {
            Codegen.generateIndexed("la", Codegen.T1, Codegen.FP, mySym.getOffset());
            //int offset = mySym.getOffset();
        }
    }

    public void codeAssign(String reg) {
        Codegen.generateIndexed("sw", reg, Codegen.FP, mySym.getOffset());
        //Codegen.generate("sw", , mySym.getOffset() + Codegen.FP)
    }

    public void unparse(PrintWriter p, int indent) {
        p.print(myStrVal);
        if (mySym != null) {
            p.print("(" + mySym + ")");
        }
    }

    private int myLineNum;
    private int myCharNum;
    private String myStrVal;
    private Sym mySym;
}

class DotAccessExpNode extends ExpNode {
    public DotAccessExpNode(ExpNode loc, IdNode id) {
        myLoc = loc;
        myId = id;
        mySym = null;
    }

    /**
     * Return the symbol associated with this dot-access node.
     */
    public Sym sym() {
        return mySym;
    }

    /**
     * Return the line number for this dot-access node.
     * The line number is the one corresponding to the RHS of the dot-access.
     */
    public int lineNum() {
        return myId.lineNum();
    }

    /**
     * Return the char number for this dot-access node.
     * The char number is the one corresponding to the RHS of the dot-access.
     */
    public int charNum() {
        return myId.charNum();
    }

    /**
     * nameAnalysis
     * Given a symbol table symTab, do:
     * - process the LHS of the dot-access
     * - process the RHS of the dot-access
     * - if the RHS is of a struct type, set the sym for this node so that
     *   a dot-access "higher up" in the AST can get access to the symbol
     *   table for the appropriate struct definition
     */
    public void nameAnalysis(SymTable symTab) {
        badAccess = false;
        SymTable structSymTab = null; // to lookup RHS of dot-access
        Sym sym = null;

        myLoc.nameAnalysis(symTab);  // do name analysis on LHS

        // if myLoc is really an ID, then sym will be a link to the ID's symbol
        if (myLoc instanceof IdNode) {
            IdNode id = (IdNode)myLoc;
            sym = id.sym();

            // check ID has been declared to be of a struct type

            if (sym == null) { // ID was undeclared
                badAccess = true;
            }
            else if (sym instanceof StructSym) {
                // get symbol table for struct type
                Sym tempSym = ((StructSym)sym).getStructType().sym();
                structSymTab = ((StructDefSym)tempSym).getSymTable();
            }
            else {  // LHS is not a struct type
                ErrMsg.fatal(id.lineNum(), id.charNum(),
                             "Dot-access of non-struct type");
                badAccess = true;
            }
        }

        // if myLoc is really a dot-access (i.e., myLoc was of the form
        // LHSloc.RHSid), then sym will either be
        // null - indicating RHSid is not of a struct type, or
        // a link to the Sym for the struct type RHSid was declared to be
        else if (myLoc instanceof DotAccessExpNode) {
            DotAccessExpNode loc = (DotAccessExpNode)myLoc;

            if (loc.badAccess) {  // if errors in processing myLoc
                badAccess = true; // don't continue proccessing this dot-access
            }
            else { //  no errors in processing myLoc
                sym = loc.sym();

                if (sym == null) {  // no struct in which to look up RHS
                    ErrMsg.fatal(loc.lineNum(), loc.charNum(),
                                 "Dot-access of non-struct type");
                    badAccess = true;
                }
                else {  // get the struct's symbol table in which to lookup RHS
                    if (sym instanceof StructDefSym) {
                        structSymTab = ((StructDefSym)sym).getSymTable();
                    }
                    else {
                        System.err.println("Unexpected Sym type in DotAccessExpNode");
                        System.exit(-1);
                    }
                }
            }

        }

        else { // don't know what kind of thing myLoc is
            System.err.println("Unexpected node type in LHS of dot-access");
            System.exit(-1);
        }

        // do name analysis on RHS of dot-access in the struct's symbol table
        if (!badAccess) {

            sym = structSymTab.lookupGlobal(myId.name()); // lookup
            if (sym == null) { // not found - RHS is not a valid field name
                ErrMsg.fatal(myId.lineNum(), myId.charNum(),
                             "Invalid struct field name");
                badAccess = true;
            }

            else {
                myId.link(sym);  // link the symbol
                // if RHS is itself as struct type, link the symbol for its struct
                // type to this dot-access node (to allow chained dot-access)
                if (sym instanceof StructSym) {
                    mySym = ((StructSym)sym).getStructType().sym();
                }
            }
        }
    }

    public void typeCheck() {
        t = myId.sym().getType();
    }

    public void codeGen() {
        // THIS WILL NEVER BE CALLED
    }

    public void unparse(PrintWriter p, int indent) {
        myLoc.unparse(p, 0);
        p.print(".");
        myId.unparse(p, 0);
    }

    // 2 kids
    private ExpNode myLoc;
    private IdNode myId;
    private Sym mySym;          // link to Sym for struct type
    private boolean badAccess;  // to prevent multiple, cascading errors
}

class AssignNode extends ExpNode {
    public AssignNode(ExpNode lhs, ExpNode exp) {
        myLhs = lhs;
        myExp = exp;
    }

    /**
     * nameAnalysis
     * Given a symbol table symTab, perform name analysis on this node's
     * two children
     */
    public void nameAnalysis(SymTable symTab) {
        myLhs.nameAnalysis(symTab);
        myExp.nameAnalysis(symTab);
    }

    public void typeCheck() {
        myLhs.typeCheck();
        myExp.typeCheck();

        if (myLhs.t.isErrorType() || myExp.t.isErrorType()) {
            t = new ErrorType();
            return;
        }

        if (!myLhs.t.equals(myExp.t)) {
           ErrMsg.fatal(myLhs.lineNum(), myLhs.charNum(), "Type mismatch");
           t = new ErrorType();
        }
        else {
            if (myLhs instanceof IdNode) {
                IdNode myId = (IdNode) myLhs;
                if (myId.sym().getType().isFnType()) {
                    ErrMsg.fatal(myId.lineNum(), myId.charNum(),"Function assignment");
                    t = new ErrorType();
                }
                else if (myId.sym().getType().isStructDefType()) {
                    ErrMsg.fatal(myId.lineNum(), myId.charNum(), "Struct name assignment");
                    t = new ErrorType();
                }
                else if (myId.sym().getType().isStructType()) {
                    ErrMsg.fatal(myId.lineNum(), myId.charNum(), "Struct variable assignment");
                    t = new ErrorType();
                }
            }
        }

        if (t == null) {
            t = myLhs.t;
        }
    }

    public int lineNum() {
        return myLhs.lineNum();
    }

    public int charNum() {
        return myLhs.charNum();
    }

    public void codeGen() {
        // generate and evaluate expression, leaving result on the stack
        myExp.codeGen();
        Codegen.generateIndexed("lw", Codegen.T0, Codegen.SP, 4);
        if (myLhs instanceof IdNode) {
            // push address of Id onto the stack
            IdNode myId = (IdNode) myLhs;

            if (myId.sym().isGlobal()) {
                Codegen.generateWithComment("sw", "ASSIGNMENT " + myId.name(), Codegen.T0, "_" + myId.name());

            }
            else {
                Codegen.generateIndexed("sw", Codegen.T0, Codegen.FP, myId.sym().getOffset(),
                        "ASSIGNMENT " + myId.name());
            }
        }
        else {
            ErrMsg.fatal(myLhs.lineNum(), myLhs.charNum(), "Unexpected assignment expression");
        }
    }

    public void unparse(PrintWriter p, int indent) {
        if (indent != -1)  p.print("(");
        myLhs.unparse(p, 0);
        p.print(" = ");
        myExp.unparse(p, 0);
        if (indent != -1)  p.print(")");
    }

    // 2 kids
    private ExpNode myLhs;
    private ExpNode myExp;
}

class CallExpNode extends ExpNode {
    public CallExpNode(IdNode name, ExpListNode elist) {
        myId = name;
        myExpList = elist;
    }

    public CallExpNode(IdNode name) {
        myId = name;
        myExpList = new ExpListNode(new LinkedList<ExpNode>());
    }

    /**
     * nameAnalysis
     * Given a symbol table symTab, perform name analysis on this node's
     * two children
     */
    public void nameAnalysis(SymTable symTab) {
        myId.nameAnalysis(symTab);
        myExpList.nameAnalysis(symTab);
    }

    public void typeCheck() {
        if (!myId.sym().getType().isFnType()) {
            ErrMsg.fatal(myId.lineNum(), myId.charNum(), "Attempt to call a non-function");
        }
        else {
            FnSym fnSym = (FnSym) myId.sym();
            if (fnSym.getNumParams() != myExpList.size()) {
                ErrMsg.fatal(myId.lineNum(), myId.charNum(), "Function call with wrong number of args");
            }
            else {
                List<Type> paramTypes = fnSym.getParamTypes();
                Iterator<Type> paramIt = paramTypes.iterator();

                List<ExpNode> myExps = myExpList.getExps();
                Iterator<ExpNode> expIt = myExps.iterator();

                while (expIt.hasNext()) {
                    ExpNode nextExp = expIt.next();
                    nextExp.typeCheck();
                    Type nextParam = paramIt.next();

                    if (!nextExp.t.isErrorType() && !nextExp.t.equals(nextParam)) {
                        ErrMsg.fatal(nextExp.lineNum(), nextExp.charNum(),
                                "Type of actual does not match type of formal");
                    }
                }
            }
            t = fnSym.getReturnType();
        }
    }


    public int lineNum() {
        return myId.lineNum();
    }

    public int charNum() {
        return myId.charNum();
    }

    public void codeGen() {
        myExpList.codeGen();  // pushes parameters on the stack
        Codegen.generate("jal", "_"+ myId.name());

        Codegen.genPush(Codegen.V0);
    }

    // ** unparse **
    public void unparse(PrintWriter p, int indent) {
        myId.unparse(p, 0);
        p.print("(");
        if (myExpList != null) {
            myExpList.unparse(p, 0);
        }
        p.print(")");
    }

    // 2 kids
    private IdNode myId;
    private ExpListNode myExpList;  // possibly null
}

abstract class UnaryExpNode extends ExpNode {
    public UnaryExpNode(ExpNode exp) {
        myExp = exp;
    }

    /**
     * nameAnalysis
     * Given a symbol table symTab, perform name analysis on this node's child
     */
    public void nameAnalysis(SymTable symTab) {
        myExp.nameAnalysis(symTab);
    }

    public int lineNum() { return myExp.lineNum(); }
    public int charNum() { return myExp.charNum(); }

    // one child
    protected ExpNode myExp;
}

abstract class BinaryExpNode extends ExpNode {
    public BinaryExpNode(ExpNode exp1, ExpNode exp2) {
        myExp1 = exp1;
        myExp2 = exp2;
    }

    /**
     * nameAnalysis
     * Given a symbol table symTab, perform name analysis on this node's
     * two children
     */
    public void nameAnalysis(SymTable symTab) {
        myExp1.nameAnalysis(symTab);
        myExp2.nameAnalysis(symTab);
    }

    /**
     * arithmeticBinaryTypeCheck
     * Type check for each operand and print error message
     * return false for no errors
     * return true if errors were reported
     */
    public static boolean arithmeticBinaryTypeCheck(ExpNode myExp1, ExpNode myExp2) {
        myExp1.typeCheck();
        myExp2.typeCheck();

        if (myExp1.t.isErrorType() || myExp2.t.isErrorType()) {
            return true;
        }

        List<ExpNode> badExps = new ArrayList<ExpNode>();
        if (!myExp1.t.isIntType()) {
            badExps.add(myExp1);
        }

        if (!myExp2.t.isIntType()) {
            badExps.add(myExp2);
        }

        for (ExpNode bad : badExps) {
            ErrMsg.fatal(bad.lineNum(), bad.charNum(), "Arithmetic operator applied to non-numeric operand");
        }

        if (badExps.isEmpty()) {
            return false;
        }

        return true;
    }

    /**
     * relationalBinaryTypeCheck
     * Type check for each operand and print error message
     * return false for no errors
     * return true if errors were reported
     */
    public static boolean relationalBinaryTypeCheck(ExpNode myExp1, ExpNode myExp2) {
        myExp1.typeCheck();
        myExp2.typeCheck();
        if (myExp1.t.isErrorType() || myExp2.t.isErrorType()) {
            return true;
        }

        List<ExpNode> badExps = new ArrayList<ExpNode>();
        if (!myExp1.t.isIntType()) {
            badExps.add(myExp1);
        }
        if (!myExp2.t.isIntType()) {
            badExps.add(myExp2);
        }
        for (ExpNode bad : badExps) {
            ErrMsg.fatal(bad.lineNum(), bad.charNum(), "Relational operator applied to non-numeric operand");
        }

        if (badExps.isEmpty()) {
            return false;
        }

        return true;
    }

    abstract void typeCheck();
    public int lineNum() { return myExp1.lineNum(); }
    public int charNum() { return myExp1.charNum(); }

    // two kids
    protected ExpNode myExp1;
    protected ExpNode myExp2;
}

// **********************************************************************
// Subclasses of UnaryExpNode
// **********************************************************************

class UnaryMinusNode extends UnaryExpNode {
    public UnaryMinusNode(ExpNode exp) {
        super(exp);
    }

    public void unparse(PrintWriter p, int indent) {
        p.print("(-");
        myExp.unparse(p, 0);
        p.print(")");
    }

    public void typeCheck() {
        myExp.typeCheck();

        if (myExp.t.isErrorType()){
            t = new ErrorType();
            return;
        }

        if (!myExp.t.isIntType()) {
            if (myExp instanceof DotAccessExpNode) {
                DotAccessExpNode myDot = (DotAccessExpNode) myExp;
                ErrMsg.fatal(myDot.lineNum(), myDot.charNum(), "Arithmetic operator applied to non-numeric operand");
                t = new ErrorType();
            }
            else {
                IdNode myId = (IdNode) myExp;
                ErrMsg.fatal(myId.lineNum(), myId.charNum(), "Arithmetic " +
                        "operator applied to non-numeric operand");
                t = new ErrorType();
            }
        }

        if (t == null) {
            t = new IntType();
        }
    }

    public void codeGen() {
        myExp.codeGen();

        Codegen.genPop(Codegen.T0);
        Codegen.generate("mulo", Codegen.T0, Codegen.T0, -1);
        Codegen.genPush(Codegen.T0);
    }
}

class NotNode extends UnaryExpNode {
    public NotNode(ExpNode exp) {
        super(exp);
    }

    public void unparse(PrintWriter p, int indent) {
        p.print("(!");
        myExp.unparse(p, 0);
        p.print(")");
    }

    public void typeCheck() {
        myExp.typeCheck();

        if (myExp.t.isErrorType()) {
            t = new ErrorType();
            return;
        }

        if (!myExp.t.isBoolType()) {
            ErrMsg.fatal(myExp.lineNum(), myExp.charNum(), "Logical operator applied to non-bool operand");
            t = new ErrorType();
            return;
        }
        t = new BoolType();
    }

    public void codeGen() {
        myExp.codeGen();
        Codegen.genPop(Codegen.T0);
        Codegen.generate("nor", Codegen.T0, Codegen.T0, 0);
        Codegen.genPush(Codegen.T0);
    }
}

// **********************************************************************
// Subclasses of BinaryExpNode
// **********************************************************************

class PlusNode extends BinaryExpNode {
    public PlusNode(ExpNode exp1, ExpNode exp2) {
        super(exp1, exp2);
    }

    public void unparse(PrintWriter p, int indent) {
        p.print("(");
        myExp1.unparse(p, 0);
        p.print(" + ");
        myExp2.unparse(p, 0);
        p.print(")");
    }

    public void typeCheck() {
        if (arithmeticBinaryTypeCheck(myExp1, myExp2)) {
            t = new ErrorType();
        }
        else {
            t = new IntType();
        }
    }

    public void codeGen() {
        myExp1.codeGen();
        myExp2.codeGen();

        // pop R and then L operand
        Codegen.genPop(Codegen.T1);
        Codegen.genPop(Codegen.T0);

        // add together and push
        Codegen.generateWithComment("add", "ADD", Codegen.T0, Codegen.T0, Codegen.T1);
        Codegen.genPush(Codegen.T0);
    }
}

class MinusNode extends BinaryExpNode {
    public MinusNode(ExpNode exp1, ExpNode exp2) {
        super(exp1, exp2);
    }

    public void unparse(PrintWriter p, int indent) {
        p.print("(");
        myExp1.unparse(p, 0);
        p.print(" - ");
        myExp2.unparse(p, 0);
        p.print(")");
    }

    public void typeCheck() {
        if (arithmeticBinaryTypeCheck(myExp1, myExp2)) {
            t = new ErrorType();
        } else {
            t = new IntType();
        }
    }

    public void codeGen() {
        myExp1.codeGen();
        myExp2.codeGen();

        // pop 1
        Codegen.genPop(Codegen.T1); // R operand
        Codegen.genPop(Codegen.T0); // L operand
        // pop 2

        // subtract and push
        Codegen.generateWithComment("sub", "SUBTRACT", Codegen.T0, Codegen.T0, Codegen.T1);
        Codegen.genPush(Codegen.T0);
    }
}

class TimesNode extends BinaryExpNode {
    public TimesNode(ExpNode exp1, ExpNode exp2) {
        super(exp1, exp2);
    }

    public void unparse(PrintWriter p, int indent) {
        p.print("(");
        myExp1.unparse(p, 0);
        p.print(" * ");
        myExp2.unparse(p, 0);
        p.print(")");
    }

    public void typeCheck() {
        if (arithmeticBinaryTypeCheck(myExp1, myExp2)) {
            t = new ErrorType();
        } else {
            t = new IntType();
        }
    }

    public void codeGen() {
        myExp1.codeGen();
        myExp2.codeGen();

        Codegen.genPop(Codegen.T1); // R operand
        Codegen.genPop(Codegen.T0); // L operand

        // multiply and push
        Codegen.generateWithComment("mulo", "MULTIPLY", Codegen.T0, Codegen.T0, Codegen.T1);
        Codegen.genPush(Codegen.T0);
    }
}

class DivideNode extends BinaryExpNode {
    public DivideNode(ExpNode exp1, ExpNode exp2) {
        super(exp1, exp2);
    }

    public void unparse(PrintWriter p, int indent) {
        p.print("(");
        myExp1.unparse(p, 0);
        p.print(" / ");
        myExp2.unparse(p, 0);
        p.print(")");
    }

    public void typeCheck() {
        if (arithmeticBinaryTypeCheck(myExp1, myExp2)) {
            t = new ErrorType();
        } else {
            t = new IntType();
        }
    }

    public void codeGen() {
        myExp1.codeGen();
        myExp2.codeGen();

        Codegen.genPop(Codegen.T1); // R operand
        Codegen.genPop(Codegen.T0); // L operand

        // divide and push
        Codegen.generateWithComment("div", "DIVIDE", Codegen.T0, Codegen.T0, Codegen.T1);
        Codegen.genPush(Codegen.T0);
    }
}

class AndNode extends BinaryExpNode {
    public AndNode(ExpNode exp1, ExpNode exp2) {
        super(exp1, exp2);
    }

    public void unparse(PrintWriter p, int indent) {
        p.print("(");
        myExp1.unparse(p, 0);
        p.print(" && ");
        myExp2.unparse(p, 0);
        p.print(")");
    }

    public void typeCheck() {
        myExp1.typeCheck();
        myExp2.typeCheck();

        if (myExp1.t.isErrorType() || myExp2.t.isErrorType()) {
            t = new ErrorType();
            return;
        }

        if (!myExp1.t.isBoolType()) {
            ErrMsg.fatal(myExp1.lineNum(), myExp1.charNum(), "Logical operator applied to non-bool operand");
            t = new ErrorType();
        }
        if (!myExp2.t.isBoolType()) {
            ErrMsg.fatal(myExp2.lineNum(), myExp2.charNum(), "Logical operator applied to non-bool operand");
            t = new ErrorType();
        }

        if (t == null) {
            t = new BoolType();
        }
    }

    public void codeGen() {
        myExp1.codeGen();
        Codegen.genPop(Codegen.T0);

        String endLabel = Codegen.nextLabel();

        Codegen.generate("beq", Codegen.T0, Codegen.FALSE, endLabel);
        myExp2.codeGen();
        Codegen.genPop(Codegen.T0);

        Codegen.genLabel(endLabel);
        Codegen.genPush(Codegen.T0);
    }
}

class OrNode extends BinaryExpNode {
    public OrNode(ExpNode exp1, ExpNode exp2) {
        super(exp1, exp2);
    }

    public void unparse(PrintWriter p, int indent) {
        p.print("(");
        myExp1.unparse(p, 0);
        p.print(" || ");
        myExp2.unparse(p, 0);
        p.print(")");
    }

    public void typeCheck() {
        myExp1.typeCheck();
        myExp2.typeCheck();

        if (myExp1.t.isErrorType() || myExp2.t.isErrorType()) {
            t = new ErrorType();
            return;
        }

        if (!myExp1.t.isBoolType()) {
            ErrMsg.fatal(myExp1.lineNum(), myExp1.charNum(), "Logical operator applied to non-bool operand");
            t = new ErrorType();
        }
        if (!myExp2.t.isBoolType()) {
            ErrMsg.fatal(myExp2.lineNum(), myExp2.charNum(), "Logical operator applied to non-bool operand");
            t = new ErrorType();
        }

        if (t == null) {
            t = new BoolType();
        }
    }

    public void codeGen() {
        myExp1.codeGen();
        Codegen.genPop(Codegen.T0);

        String endLabel = Codegen.nextLabel();

        Codegen.generate("beq", Codegen.T0, Codegen.TRUE, endLabel);
        myExp2.codeGen();
        Codegen.genPop(Codegen.T0);

        Codegen.genLabel(endLabel);
        Codegen.genPush(Codegen.T0);
    }
}

class EqualsNode extends BinaryExpNode {
    public EqualsNode(ExpNode exp1, ExpNode exp2) {
        super(exp1, exp2);
    }

    public void unparse(PrintWriter p, int indent) {
        p.print("(");
        myExp1.unparse(p, 0);
        p.print(" == ");
        myExp2.unparse(p, 0);
        p.print(")");
    }

    public void typeCheck() {
        myExp1.typeCheck();
        myExp2.typeCheck();

        if (myExp1.t.isErrorType() || myExp2.t.isErrorType()) {
            t = new ErrorType();
            return;
        }

        if (!myExp1.t.equals(myExp2.t)) {
            ErrMsg.fatal(myExp1.lineNum(), myExp1.charNum(), "Type mismatch");
            t = new ErrorType();
        }
        else if (myExp1.t.isVoidType() && myExp2.t.isVoidType()) {
            ErrMsg.fatal(myExp1.lineNum(), myExp1.charNum(), "Equality operator applied to void functions");
            t = new ErrorType();
        }
        else if (myExp1.t.isFnType() && myExp2.t.isFnType()) {
            ErrMsg.fatal(myExp1.lineNum(), myExp1.charNum(), "Equality operator applied to functions");
            t = new ErrorType();
        }
        else if (myExp1.t.isStructDefType() && myExp2.t.isStructDefType()) {
            ErrMsg.fatal(myExp1.lineNum(), myExp1.charNum(), "Equality operator applied to struct names");
            t = new ErrorType();
        }
        else if (myExp1.t.isStructType() && myExp2.t.isStructType()) {
            ErrMsg.fatal(myExp1.lineNum(), myExp1.charNum(), "Equality operator applied to struct variables");
            t = new ErrorType();
        }
        else {
            t = new BoolType();
        }
    }

    public void codeGen() {
            myExp1.codeGen();
            myExp2.codeGen();

            Codegen.genPop(Codegen.T1);
            Codegen.genPop(Codegen.T0);

            String trueLabel = Codegen.nextLabel();
            String endLabel = Codegen.nextLabel();

            Codegen.generate("beq", Codegen.T0, Codegen.T1, trueLabel);
            Codegen.generate("li", Codegen.T0, Codegen.FALSE);
            Codegen.generate("b", endLabel);

            Codegen.genLabel(trueLabel);
            Codegen.generate("li", Codegen.T0, Codegen.TRUE);

            Codegen.genLabel(endLabel);
            Codegen.genPush(Codegen.T0);
    }
}

class NotEqualsNode extends BinaryExpNode {
    public NotEqualsNode(ExpNode exp1, ExpNode exp2) {
        super(exp1, exp2);
    }

    public void unparse(PrintWriter p, int indent) {
        p.print("(");
        myExp1.unparse(p, 0);
        p.print(" != ");
        myExp2.unparse(p, 0);
        p.print(")");
    }

    public void typeCheck() {
        myExp1.typeCheck();
        myExp2.typeCheck();

        if (myExp1.t.isErrorType() || myExp2.t.isErrorType()) {
            t = new ErrorType();
            return;
        }

        if (!myExp1.t.equals(myExp2.t)) {
            ErrMsg.fatal(myExp1.lineNum(), myExp1.charNum(), "Type mismatch");
            t = new ErrorType();
        }
        else if (myExp1.t.isVoidType() && myExp2.t.isVoidType()) {
            ErrMsg.fatal(myExp1.lineNum(), myExp1.charNum(), "Equality operator applied to void functions");
            t = new ErrorType();
        }
        else if (myExp1.t.isFnType() && myExp2.t.isFnType()) {
            ErrMsg.fatal(myExp1.lineNum(), myExp1.charNum(), "Equality operator applied to functions");
            t = new ErrorType();
        }
        else if (myExp1.t.isStructDefType() && myExp2.t.isStructDefType()) {
            ErrMsg.fatal(myExp1.lineNum(), myExp1.charNum(), "Equality operator applied to struct names");
            t = new ErrorType();
        }
        else if (myExp1.t.isStructType() && myExp2.t.isStructType()) {
            ErrMsg.fatal(myExp1.lineNum(), myExp1.charNum(), "Equality operator applied to struct variables");
            t = new ErrorType();
        }
        else {
            t = new BoolType();
        }
    }

    public void codeGen() {
        myExp1.codeGen();
        myExp2.codeGen();

        Codegen.genPop(Codegen.T1);
        Codegen.genPop(Codegen.T0);

        String trueLabel = Codegen.nextLabel();
        String endLabel = Codegen.nextLabel();

        Codegen.generate("bne", Codegen.T0, Codegen.T1, trueLabel);
        Codegen.generate("li", Codegen.T0, Codegen.FALSE);
        Codegen.generate("b", endLabel);

        Codegen.genLabel(trueLabel);
        Codegen.generate("li", Codegen.T0, Codegen.TRUE);

        Codegen.genLabel(endLabel);
        Codegen.genPush(Codegen.T0);
    }
}

class LessNode extends BinaryExpNode {
    public LessNode(ExpNode exp1, ExpNode exp2) {
        super(exp1, exp2);
    }

    public void unparse(PrintWriter p, int indent) {
        p.print("(");
        myExp1.unparse(p, 0);
        p.print(" < ");
        myExp2.unparse(p, 0);
        p.print(")");
    }

    public void typeCheck() {
        if (relationalBinaryTypeCheck(myExp1, myExp2)) {
            t = new ErrorType();
        }
        else {
            t = new BoolType();
        }
    }

    public void codeGen() {
        myExp1.codeGen();
        myExp2.codeGen();

        Codegen.genPop(Codegen.T1);
        Codegen.genPop(Codegen.T0);

        String trueLabel = Codegen.nextLabel();
        String endLabel = Codegen.nextLabel();

        Codegen.generate("blt", Codegen.T0, Codegen.T1, trueLabel);
        Codegen.generate("li", Codegen.T0, Codegen.FALSE);
        Codegen.generate("b", endLabel);

        Codegen.genLabel(trueLabel);
        Codegen.generate("li", Codegen.T0, Codegen.TRUE);

        Codegen.genLabel(endLabel);
        Codegen.genPush(Codegen.T0);
    }
}

class GreaterNode extends BinaryExpNode {
    public GreaterNode(ExpNode exp1, ExpNode exp2) {
        super(exp1, exp2);
    }

    public void unparse(PrintWriter p, int indent) {
        p.print("(");
        myExp1.unparse(p, 0);
        p.print(" > ");
        myExp2.unparse(p, 0);
        p.print(")");
    }

    public void typeCheck() {
        if (relationalBinaryTypeCheck(myExp1, myExp2)) {
            t = new ErrorType();
        }
        else {
            t = new BoolType();
        }
    }

    public void codeGen() {
        myExp1.codeGen();
        myExp2.codeGen();

        Codegen.genPop(Codegen.T1);
        Codegen.genPop(Codegen.T0);

        String trueLabel = Codegen.nextLabel();
        String endLabel = Codegen.nextLabel();

        Codegen.generate("bgt", Codegen.T0, Codegen.T1, trueLabel);
        Codegen.generate("li", Codegen.T0, Codegen.FALSE);
        Codegen.generate("b", endLabel);

        Codegen.genLabel(trueLabel);
        Codegen.generate("li", Codegen.T0, Codegen.TRUE);

        Codegen.genLabel(endLabel);
        Codegen.genPush(Codegen.T0);
    }
}

class LessEqNode extends BinaryExpNode {
    public LessEqNode(ExpNode exp1, ExpNode exp2) {
        super(exp1, exp2);
    }

    public void unparse(PrintWriter p, int indent) {
        p.print("(");
        myExp1.unparse(p, 0);
        p.print(" <= ");
        myExp2.unparse(p, 0);
        p.print(")");
    }

    public void typeCheck() {
        if (relationalBinaryTypeCheck(myExp1, myExp2)) {
            t = new ErrorType();
        }
        else {
            t = new BoolType();
        }
    }

    public void codeGen() {
        myExp1.codeGen();
        myExp2.codeGen();

        Codegen.genPop(Codegen.T1);
        Codegen.genPop(Codegen.T0);

        String trueLabel = Codegen.nextLabel();
        String endLabel = Codegen.nextLabel();

        Codegen.generate("ble", Codegen.T0, Codegen.T1, trueLabel);
        Codegen.generate("li", Codegen.T0, Codegen.FALSE);
        Codegen.generate("b", endLabel);

        Codegen.genLabel(trueLabel);
        Codegen.generate("li", Codegen.T0, Codegen.TRUE);

        Codegen.genLabel(endLabel);
        Codegen.genPush(Codegen.T0);
    }
}

class GreaterEqNode extends BinaryExpNode {
    public GreaterEqNode(ExpNode exp1, ExpNode exp2) {
        super(exp1, exp2);
    }

    public void unparse(PrintWriter p, int indent) {
        p.print("(");
        myExp1.unparse(p, 0);
        p.print(" >= ");
        myExp2.unparse(p, 0);
        p.print(")");
    }

    public void typeCheck() {
        if (relationalBinaryTypeCheck(myExp1, myExp2)) {
            t = new ErrorType();
        }
        else {
            t = new BoolType();
        }
    }

    public void codeGen() {
        myExp1.codeGen();
        myExp2.codeGen();

        Codegen.genPop(Codegen.T1);
        Codegen.genPop(Codegen.T0);

        String trueLabel = Codegen.nextLabel();
        String endLabel = Codegen.nextLabel();

        Codegen.generate("bge", Codegen.T0, Codegen.T1, trueLabel);
        Codegen.generate("li", Codegen.T0, Codegen.FALSE);
        Codegen.generate("b", endLabel);

        Codegen.genLabel(trueLabel);
        Codegen.generate("li", Codegen.T0, Codegen.TRUE);

        Codegen.genLabel(endLabel);
        Codegen.genPush(Codegen.T0);
    }
}