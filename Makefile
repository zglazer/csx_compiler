###
# This Makefile can be used to make a compiler for the CSX language
# (ASTnode.class) and to make a program (P6.class) that tests all the
# methods in ast.java.
#
# make clean removes all generated files.
#
###

JC = javac

P6.class: P6.java parser.class Yylex.class ASTnode.class
	$(JC) -g P6.java

ASTnode.class: ast.java EmptySymTableException.class DuplicateSymException.class 
	$(JC) -g ast.java Type.java SymTable.java Sym.java Codegen.java

Sym.class: Sym.java ASTnode.class
	$(JC) -g Sym.java

SymTable.class: Sym.class SymTable.java
	$(JC) -g SymTable.java

EmptySymTableException.class: EmptySymTableException.java
	$(JC) -g EmptySymTableException.java

DuplicateSymException.class: DuplicateSymException.java
	$(JC) -g DuplicateSymException.java

parser.class: parser.java ASTnode.class Yylex.class ErrMsg.class
	$(JC) parser.java

parser.java: CSX.cup
	java java_cup.Main < CSX.cup

Yylex.class: CSX.jlex.java sym.class ErrMsg.class
	$(JC) CSX.jlex.java

CSX.jlex.java: CSX.jlex sym.class
	java JLex.Main CSX.jlex

sym.class: sym.java 
	$(JC) -g sym.java

sym.java: CSX.cup
	java java_cup.Main < CSX.cup

ErrMsg.class: ErrMsg.java
	$(JC) ErrMsg.java

###
# clean
###
clean:
	rm -f *~ *.class parser.java CSX.jlex.java sym.java *.out
